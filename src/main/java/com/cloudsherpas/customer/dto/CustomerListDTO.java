package com.cloudsherpas.customer.dto;

/**
 * Created by lbarretto on 10/15/15.
 */
import java.util.List;

public class CustomerListDTO {

    private List<CustomerDTO> items;

    public List<CustomerDTO> getItems() {
        return items;
    }

    public void setItems(List<CustomerDTO> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "CustomerListDTO{" +
                "items=" + items +
                '}';
    }
}

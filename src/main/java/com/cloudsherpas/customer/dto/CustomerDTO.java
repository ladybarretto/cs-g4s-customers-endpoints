package com.cloudsherpas.customer.dto;

import java.util.Date;

/**
 * Created by lbarretto on 10/8/15.
 */
public class CustomerDTO {

    private Long id;
    private String customerName;
    private String description;
    private String emailAddress;
    private Date dateAdded;
    private String phoneNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + id +
                ", customerName='" + customerName + '\'' +
                ", description='" + description + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", dateAdded=" + dateAdded +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

}

package com.cloudsherpas.customer.api;

import com.cloudsherpas.customer.dto.CustomerDTO;
import com.cloudsherpas.customer.dto.CustomerListDTO;
import com.cloudsherpas.customer.service.CustomerService;
import com.cloudsherpas.customer.util.CustomerErrorEndpointMessage;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(
        name = "customer",
        version = "v1",
        namespace = @ApiNamespace(ownerDomain = "cloudsherpas.com", ownerName = "CloudSherpas"),
        description = "Sample API for Google Cloud Endpoints"
)
public class CustomerResource {

    @Autowired
    @Lazy
    @Qualifier("customerService")
    private CustomerService customerService;

    @ApiMethod(
            name = "get",
            path = "customer",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public CustomerDTO getCustomer(@Named("id") final Long key) throws NotFoundException {

        final CustomerDTO customer = customerService.getCustomer(key);

        // Data not found
        if (customer == null){
            CustomerErrorEndpointMessage.entityNotFound();
        }

        return customer;
    }

    @ApiMethod(
            name = "getAll",
            path = "customers/all",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public List<CustomerDTO> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @ApiMethod(
            name = "add",
            path = "customer/add",
            httpMethod = ApiMethod.HttpMethod.PUT
    )
    public Map<String, Long> addCustomer(final CustomerDTO customerDTO) {
        final Long key = customerService.addUpdateCustomer(customerDTO);
        final Map<String, Long> result = new HashMap<>();

        if (key != null){
            result.put("key", key);
        }

        return result;
    }

    @ApiMethod(
            name = "delete",
            path = "customer/delete",
            httpMethod = ApiMethod.HttpMethod.DELETE
    )
    public void deleteCustomer(@Named("id") final Long key) {
        customerService.deleteCustomer(key);
    }

    @ApiMethod(
            name = "deleteSelected",
            path = "customer/deleteSelected",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public void deleteSelectedCustomers(final CustomerListDTO customerList) {
        final List<CustomerDTO> customers = customerList.getItems();
        for (CustomerDTO customerDTO : customers){
            customerService.deleteCustomer(customerDTO.getId());
        }
    }
}

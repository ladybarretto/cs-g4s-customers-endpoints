package com.cloudsherpas.customer.dao.impl;

import com.cloudsherpas.customer.dao.CustomerDao;
import com.cloudsherpas.customer.model.Customer;

/**
 * Created by lbarretto on 10/7/15.
 */
public class CustomerDaoImpl extends BaseDaoImpl<Customer> implements CustomerDao{

    public CustomerDaoImpl(){
        super(Customer.class);
    }
}


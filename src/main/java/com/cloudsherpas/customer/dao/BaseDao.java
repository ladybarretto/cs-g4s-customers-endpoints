package com.cloudsherpas.customer.dao;

import java.util.List;

/**
 * Created by lbarretto on 10/7/15.
 */
public interface BaseDao<T> {

    T get(Long key);

    List<T> getAll();

    Long put(T entity);

    List<T> putAll(List<T> entities);

    void delete(Long key);

}

package com.cloudsherpas.customer.dao;

import com.cloudsherpas.customer.model.Customer;

/**
 * Created by lbarretto on 10/7/15.
 */
public interface CustomerDao extends BaseDao<Customer>{

}

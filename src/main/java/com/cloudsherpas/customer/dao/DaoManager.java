package com.cloudsherpas.customer.dao;

import com.cloudsherpas.customer.model.Customer;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * Created by lbarretto on 10/7/15.
 */
public class DaoManager {

    private static DaoManager self;

    static {
        registerEntities();
    }

    private DaoManager() {
    }
    public static DaoManager getInstance(){

        if (self == null) {
            self = new DaoManager();
        }

        return self;

    }

    private static void registerEntities(){
        ObjectifyService.begin();
        ObjectifyService.factory().register(Customer.class);
    }

    public Objectify getObjectify(){
        return ObjectifyService.ofy();
    }
}

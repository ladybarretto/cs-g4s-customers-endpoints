package com.cloudsherpas.customer.util;

import com.google.api.server.spi.response.NotFoundException;

/**
 * Created by lbarretto on 10/8/15.
 */
public final class CustomerErrorEndpointMessage {

    private CustomerErrorEndpointMessage(){}

    public static void entityNotFound() throws NotFoundException{
        final String message = "Data not found.";
        entityNotFound(message);
    }

    public static void entityNotFound(final String message) throws NotFoundException {
        throw new NotFoundException(message);
    }
}

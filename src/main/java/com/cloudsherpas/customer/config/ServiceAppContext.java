package com.cloudsherpas.customer.config;

import com.cloudsherpas.customer.service.CustomerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@Lazy
public class ServiceAppContext {

    @Bean(name="customerService")
    public CustomerService getCustomerService(){
        return new CustomerService();
    }

}

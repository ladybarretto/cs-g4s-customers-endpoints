package com.cloudsherpas.customer.config;

import com.cloudsherpas.customer.dao.CustomerDao;
import com.cloudsherpas.customer.dao.impl.CustomerDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@Lazy
public class DaoAppContext {

    @Bean(name="customerDao")
    public CustomerDao getCustomerDao(){
        return new CustomerDaoImpl();
    }
}
